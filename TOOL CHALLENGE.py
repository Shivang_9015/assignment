# IMPORTING ALL NECCESSORY MODULES
from types import new_class
from typing import Sized
import requests
import pandas
import json
import json
from collections import namedtuple
from json import JSONEncoder
from collections import OrderedDict



def customStudentDecoder(studentDict):
    return namedtuple('X', studentDict.keys())(*studentDict.values())

#----------------------------------------------------------------------------------------------------------------
##SCAN DIRECTORY THROUGH GITLAB API's BY PROJECT ID AND ACCESS TOKEN

result = requests.get('http://evgit/api/v4/projects/15592?private_token=glpat-CP5grfCMRczzmhBC35bN')  ## Api request to scan the directory in gitlab
member = requests.get('http://evgit/api/v4/projects/15592/members?private_token=glpat-CP5grfCMRczzmhBC35bN') ## member detail api
events = requests.get('http://evgit/api/v4/projects/15592/events?private_token=glpat-CP5grfCMRczzmhBC35bN') ## events detail api

#------------------------------------------------------------------------------------------------------------------------------------------------
## PROCESSING RAW DATA INTO JSON FORMAT AND FURTHER PUT IN DICTIONARY
data = result.text
member_api = member.text
events_api = events.text
json_data =  json.loads(data)
json_member = json.loads(member_api)
json_events = json.loads(events_api)

##CREATING THE DICTIONARY WHICH KEPT ALL REQUIRED TABLE DETAILS
detail_list = {}



#----------------------------------------------------------------------------------------------------------------------------------
##MEMBERS WHO CHECKIN CODE  NAME
member_name
print("Team member who have checked in code:")  ## printing the no of member who checkin code
for x in range(len(json_member)):
    member_detail = json_member[x]
    for key, value in member_detail.items():
        key_ = {key}
        if(key_ == {'name'}):
            member_name = value
            
##----------------------------------------------------------------------------------------------------------------------------------  

## MENTOR NAME DETAIL
try:
    mentorsData = pandas.read_excel('Mentor_Mapping.xls') #dataframe to store data from excel sheet where data of mentors is stored
except:
    print('could not find mentor data')

def getMentorDetails(targetEmployeeEmail):
    if (mentorsData.get('EV ID') is None ): 
        return('Unable to find')  
    for email, mentorName in zip(mentorsData.get('EV Mail Id'), mentorsData.get('Mentor')):   #retrieve the email and mentor name from excel sheet dataframe
        if email==targetEmployeeEmail:  #check if email in mentor sheet matches the passed email(email of committer)
            return mentorName     #return mentor name for the committer email if match is found
    return ('Not Assigned')


#-------------------------------------------------------------------------------------------------------------------------------------
   

            
##EXTRACTING TABLE DETAIL FROM API's   
for x in range(len(json_events)):    ## extracting table detail from Api data
    events_detail = json_events[x]
    
    No_of_files = None
    Last_checkin = None
    comment = None

    for key, value in events_detail.items():
        key_ = {key}
        if(key_ == {'created_at'}):   ## Last Checkin date
            Last_checkin = value
            
        
        if(key_ == {'push_data'}):
            file_detail = value

            for key in file_detail:
                if(key == 'commit_title'):  ## Comment detail
                    comment = file_detail[key]

            for key in file_detail:   ##  No of files
                if(key == 'commit_count'):
                    No_of_files = file_detail[key]
                   
    Mentor_Name = getMentorDetails(member_name)
    
    if(No_of_files != None):
        detail_list[No_of_files] = (Last_checkin,comment,Mentor_Name)
#----------------------------------------------------------------------------------------------------------------------------------
##SORTING TABLE BY MAX FILES
detail_list_temp = OrderedDict(reversed(list(detail_list.items())))
detail_list_sorted = dict(reversed(list(detail_list_temp.items())))


#-------------------------------------------------------------------------------------------------------------------------------------

##PRINTING TABLE INFORMATION DETAIL
print("Table")
print("No_of_files           Last Checkin               Review Comment                Mentor Name")
print("---------------------------------------------------------------------------------------------------------")
for item in detail_list_sorted:
    print(item,"          ",detail_list_sorted[item][0],"        ",detail_list_sorted[item][1],"                ",Mentor_Name)


#===========================================END================================================================================================







    




   

            



