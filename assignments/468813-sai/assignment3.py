"""Develop a OOP find grade of a Student who appears in exam
for 5 subjects.
avg - below - 50 - D
- 50 to 60 - C
- 60-75 - B
- 75-90 - A
- >90 - S
"""
class Student:
    def __init__(self,name,stuid,sub1,sub2,sub3,sub4,sub5):
        self.name=name
        self.stuid=stuid
        self.sub1=sub1
        self.sub2=sub2
        self.sub3=sub3
        self.sub4=sub4
        self.sub5=sub5
    def findgrade(self):
        avg=(self.sub1+self.sub2+self.sub3+self.sub4+self.sub5)//5
        if avg > 90:
            return 'S'
        elif avg<=90 and avg>75:
            return 'A'
        elif avg<=75 and avg>60:
            return 'B'
        elif avg<=60 and avg<50:
            return 'C'
        else:
            return 'D'

if __name__=="__main__":
    s1=Student("Sasidhar","170030713",80,70,90,85,75)
    s2=Student("Mythresh","170030652",75,70,90,85,80)
    print(s2.findgrade())
