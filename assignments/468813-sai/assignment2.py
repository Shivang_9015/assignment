"""
Create a dictionary
- The key is number
- Convert the key to equivalent HexaDecimal as store it as a value
"""
def findhex(key):#function to convert
    hexvalues=['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'] #values for hexadecimal
    hexdecimal=''
    while key>0:
        r=key%16
        hexdecimal=hexvalues[r]+hexdecimal
        print(hexdecimal,end=' ')
        key=key//16
    return hexdecimal #returning converted value
dectohex={
}
n=int(input()) #number of elements should be in dictionary
i=0
while i<n:
    key=int(input())
    dectohex[key]=findhex(key) #calling findhex function to convert decimal to hexadecimal
    i=i+1
print(dectohex)
