#  Ineritance and Polymorphism
#  Dynamic polymorphism, Limited operator overloading
#  method overloading and encapsulation not supported
#  python does not have support for access specifiers
#  In python every entity is public
#  Enforce encapsulation  - programmer uses convention
#  private entity name has '_' underscore as prefix
'''
how to create a class in python
class class_name(mother_class1,mother_class2...):
    attributes....
    .....
'''
# constructor doesnot have the same name as the class
# __init__() : Is the constructor for the given class it is executed when object is created

class Myclass:
    i=78
    def __init__(self,i):
        print('inside the constr')
        self.i=i  # On the current object a variable is created
    def Show(self):
        print("Inside show i:",self.i)

    def __del__(self): # Finalizer or similar to destructor
        print("The object can be de-initialized here",self.i)

# self is not a keyword
# self is the current object on which the call has been made

obj = Myclass(108)
obj2 = Myclass("Suraj")
obj3 = Myclass(True)

obj.Show()
obj2.Show()
obj3.Show()

del obj2 # It would internally call - __del__()

# python internally has - virtual machine - garbage collector
# Programmer need not have to - manage the memory

print(Myclass.i) # to access class level value