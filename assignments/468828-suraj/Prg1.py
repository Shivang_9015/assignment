import math

dict = {"suraj":3479,"kishor":3480}
for nam,val in dict.items():
    print(nam,val)
print(dict)
ans=[]

def convert_to_Hexadecimal(num):
    value=0
    n = math.log(num,16)
    n = int(n)
    while(n>=0):
        q = int(num/pow(16,n))
        num = num-pow(16,n)*q
        ans.append(q)
        value = value*10 + q
        n=n-1
    return value

deci_number=[5000,3479,345688,1234]
dict_ans={}
for i in deci_number:
    val=convert_to_Hexadecimal(i)
    dict_ans[i]=val

print(convert_to_Hexadecimal(5000))
print(dict_ans)