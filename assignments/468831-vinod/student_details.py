import xlsxwriter as xl

class StudentsData():
    """
        Class to read data from console and save to xlsx file
    """
    def __init__(self):

        ## open the exel file
        self.workbook = xl.Workbook('Student Details.xlsx')
        ## add sheets
        self.normal_sheet = self.workbook.add_worksheet()
        self.sorted_sheet = self.workbook.add_worksheet()
        ## define columns in sheet
        self.columns = ("Name", "Age", "Date of Birth", 
                   "Gender", "Address Line 1", "Address Line 2", 
                   "Address Line 3", "Pincode", "Stream"
                  )
        ## add columns to sheet
        self.add_columns()
        
        ## initiate the data variable
        self.student_data = None

    ## function to write columns to sheet    
    def add_columns(self):
        
        row = 0
        col = 0
        
        for i in self.columns:
            self.normal_sheet.write(row, col, i)
            self.sorted_sheet.write(row, col, i)
            col+=1
    
    ## function to save the data to xlsx file
    def save_data(self, data, sheet):
        row = 1
        for i in data:
            col = 0
            for ii in i:
                sheet.write(row, col, ii)
                col += 1
            row += 1

    ## funxtion to read data from console and save to xlsx file        
    def read_data_and_save(self, number_students):
        
        print(f"Please Enter {number_students} students data : ")
        
        self.student_data = tuple()
        
        for i in range(number_students):
            details  = (input("name : "), int(input("age : ")), 
                        input("date of birth (DD-MM-YYYY) : "),
                        input("gender(male/female/other/na) : "), 
                        input("address line 1 : "),
                        input("address line 2 : "),
                        input("address line 3 : "),
                        input("pincode : "),
                        input("stream : ")
                       )
            self.student_data += (details,)
            
        self.save_data(self.student_data, self.normal_sheet)
    
    
    ## function to sort data and save to file       
    def sort_data_and_save(self):
        
        if self.student_data == None:
            print("Please Enter data to sort")
            return
        
        sorted_data = tuple(sorted(self.student_data, key=lambda x: x[1]))
        
        self.save_data(sorted_data, self.sorted_sheet)
        
        print("\nThe Sorted data by age : \n", sorted_data, "\n\n")


if __name__ == '__main__':

    s = StudentsData()
    s.read_data_and_save(10)
    s.sort_data_and_save()
    s.workbook.close()