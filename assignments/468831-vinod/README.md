# Python code
> Code for writing data to .xlsx file

# Steps to run
> pyhton version used 3.8

1. install dependencies (either in virtual env or base).
    ```sh:
    $ pip install -r requirements.txt
    ```
2. Run the script
    ```sh:
    $ python student_details.py
    ```