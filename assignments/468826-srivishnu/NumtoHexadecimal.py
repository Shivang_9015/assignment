hexadecimal_dict = {}
hex_values = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']
def num_to_hexadecimal(n):
    'This method is responsible for decimal to hexadecimal conversion'
    hexadecimal_value = ''
    while(n>0):
        rem = n%16
        hexadecimal_value = hex_values[rem] + hexadecimal_value
        n = n//16
    return hexadecimal_value
while(True):
    x = int(input('Press 1 to enter a number,9 to print the dictionary 0 to exit'))
    if x==0:
        break
    elif x==9:
        print(hexadecimal_dict)
    elif x==1:
        value = int(input('Enter number'))
        hexadecimal_dict[value] = num_to_hexadecimal(value)
    else:
        print('Choose correct option')
