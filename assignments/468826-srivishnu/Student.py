class Student:
    'This class is responsible for holding students data'
    def __init__(self,name,sub1,sub2,sub3,sub4,sub5):
        self.name = name
        self.subject1 = sub1
        self.subject2 = sub2
        self.subject3 = sub3
        self.subject4 = sub4
        self.subject5 = sub5
        self.grade = None
        self.calculate_average()

    def calculate_average(self):
        'This function is responsible for calculating average and awarding the grade'
        sum = self.subject1+self.subject2+self.subject3+self.subject4+self.subject5
        avg = sum/5
        if avg>=90:
            self.grade = 'S'
        elif avg>=75 and avg<90:
            self.grade = 'A'
        elif avg>=60 and avg<75:
            self.grade = 'B'
        elif avg>=50 and avg<60:
            self.grade = 'C'
        elif avg<50:
            self.grade = 'D'
        else:
            pass

    def getName(self):
        return self.name
    def getGrade(self):
        return self.grade

if __name__ == '__main__':
    student_details = []
    while(True):
        x = int(input('Press 1 to enter a student details, 9 to print student details, 0 to exit'))
        if x==1:
            name = input('Enter name of the student: ')
            sub1 = int(input('Enter marks in Subject1: '))
            sub2 = int(input('Enter marks in Subject2: '))
            sub3 = int(input('Enter marks in Subject3: '))
            sub4 = int(input('Enter marks in Subject4: '))
            sub5 = int(input('Enter marks in Subject5: '))
            student = Student(name,sub1,sub2,sub3,sub4,sub5)
            student_details.append(student)
        elif x==9:
            for student in student_details:
                print('Name: ',student.getName(),' Grade:',student.getGrade())
        elif x==0:
            break
