#DEVELOPE A OOP TO FIND GRADE OF A STUDENT WHO APPEARS IN A EXAM FOR 5 SUBJECT
#AVERAGE BELOW 50 -D
#        51 TO 60 -C
#        61 TO 75 -B
#        76 TO 90 -A
#        ABOVE 90 -EX


class student:
    # init method or constructor
    def __init__(self,name, marks=[]):
        self.name= name
        self.marks=marks
        
    # function to find grade based on marks obtained
    def findgrade(self,sum):
        avg = sum//5
        if avg > 90:
            return 'EX'
        elif avg>75 and avg<=90:
            return 'A'
        elif avg>60 and avg<=75:
            return 'B'
        elif avg>50 and avg<=60:
            return 'C'
        else:
            return 'D'
        
        

name = input("Name of the student: ")
print(name)

marks=[]  #list to store marks of 5 subject
sum=0     #Total marks obtained in 5 subject

for i in range (0,5):
    print("Marks of subject ", i+1, ": ")
    score= int(input())
    marks.append(score)
    sum+=marks[i]

s1= student(name, marks) #object of student defined
print("Grade Obtained :  ", s1.findgrade(sum))


    
