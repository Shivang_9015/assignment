#DEVELOPE A PYTHON PROGRAM USING LIST TO FIND IF THE GIVEN No. IS ARMSTRONG NUMBER OR NOT

#list to store the input numbers
list= []

n= int(input("Enter the total number of input elements to be checked: "))

for i in range(0, n):
    ele =int(input())
    list.append(ele)

#dictionary to store input number and output as key value pair
arms_dict ={}

for i in range(0, n):
    temp= list[i]
    sum=0
    while temp>0:
        d=temp%10;
        sum+=d**3;
        temp//=10;
    if sum==list[i]:
        arms_dict[list[i]]= 'Yes'
    else:
        arms_dict[list[i]]= 'No' 

#output display as a key-value pair with Yes- Armstron No. and No- Not an Armstrong No.
print(arms_dict) 
