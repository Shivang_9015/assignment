#For a given dictionary- NumHexaDeci
#  ---The Key is number
#  ---Convert the key to equivalent HexaDecimal as store it as a value

#list to store the decimal numbers
list= []

n= int(input("Enter the number of inputs(decimal numbers) to find equivalent Hexa value : "))

for i in range(0, n):
    ele =int(input())
    list.append(ele)

#dictionary to store decimal nmber and its equivalent hexa value as key value pair
dict ={}

#function to reverse a string
def reverse(str):
    string= "".join(reversed(str))
    return string

#function to covert decimal number to hexa value
def decToHexa(n):
    
    hexaEquivalent=''
    
    while(n!=0):
        temp=0
        temp=n%16
        
        if(temp<10):
            hexaEquivalent+=chr(temp+48)
        else:
            hexaEquivalent+=chr(temp+55)
        n= int(n/16)
        
    hexaEquivalent= reverse(hexaEquivalent)
    
    return hexaEquivalent
    
    
for i in range(0,n):
    dict[list[i]]= decToHexa(list[i])
    
print("Decimal: Hexa Values ::",dict)
        


