def hexa(n,mydict):
    remainder=[]
    while (n>16):
        x=n//16
        remainder.append(str(n%16))
        n=x
    res=[]
    if (x>9):
        res.append(mydict[str(x)])
    else:
        res.append(str(x))
    for i in remainder[::-1]:
        if (int(i)>9):
            res.append(mydict[i])
        else:
            res.append(i)
    return ''.join(res)
        
numlist=[1349,1546,2465,1876,1098,3789,5467]
mydict={'10':'A','11':'B','12':'C','13':'D','14':'E','15':'F'}
dex_to_hexlist=[]
for i in numlist:
    dex_to_hexlist.append(hexa(i,mydict))
print(dex_to_hexlist)

