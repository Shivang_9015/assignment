class student:
    def __init__(self,phy,chem,math,eng,sst):
        self.phy=phy
        self.chem=chem
        self.math=math
        self.eng=eng
        self.sst=sst
    def grades(self):
        # calculating average of marks
        av=(self.phy+self.chem+self.math+self.eng+self.sst)//5
        if(av>90):
            return 'S'
        elif (av<90 and av>75):
            return 'A'
        elif (av<75 and av>60):
            return 'B'
        elif (av  <60 and av >50):
            return 'C'
        elif (av<50):
            return 'D'
        
     
print('enter no of student marks you have to enter')
n=int(input())
for i in range(n): 
    print('enter marks of subjects physics chemistry maths english Sst ')
    l=list(map(int,input().split(' ')))
    obj=student(l[0],l[1],l[2],l[3],l[4])
    print(obj.grades())
